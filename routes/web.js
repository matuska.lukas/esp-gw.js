/**
 * The entry router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Controllers
 */
const pageController = require('../controllers/page');
const errorController = require('../controllers/error');
const espController = require('../controllers/esp');

/**
 * Libs
 */
const moment = require('moment');
const numberFormat = require('../libs/numberFormat');

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment,
        numberFormat,
    };

    // move to the next route
    next();
});

router.get('/', (req, res) => {
    pageController.controller(req, res)
})

router.get('/*', (req, res) => {
    espController.sendData(CONFIG.IPsOfESPs, {
        url: req.originalUrl,
        method: 'GET',
    }, (err, status) => {
        if (err != null) {
            console.error(err);
            return res.status(500).send(err);
        } else {
            return res.send(status);
        }
    });
});



/*router.get('*', (req, res) => {
    errorController.error404(req, res);
});*/

module.exports = router;