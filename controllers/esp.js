/**
 * ESP controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const http = require('http');
var waitUntil = require('wait-until');

/**
 * Models
 */

module.exports.sendData = (IPs, data, callback) => {
    var responses = [];
    if (IPs === undefined) {
        return callback('ips-undefined');
    } else if (typeof IPs != 'object') {
        return callback('ips-not-object');
    } else if (IPs.length <= 0) {
        return callback('ips-zero-length')
    } else {
        if (data === undefined) {
            return callback('data-undefined');
        } else {
            for (let i = 0; i < IPs.length; i++) {
                const options = {
                    hostname: String(IPs[i]),
                    port: 80,
                    path: data.url,
                    method: data.method,
                }

                const req = http.request(options, res => {
                    // console.log(`statusCode: ${res.statusCode}`)

                    res.on('data', d => {
                        // process.stdout.write(d)
                        responses.push(d);
                    });
                })

                req.on('error', error => {
                    return callback(error);
                })

                req.end();
            }

            /*            while (IPs.length != responses.length) {
                            console.log('');
                        }*/
            waitUntil()
                .interval(500)
                .times(10)
                .condition(function () {
                    // console.log(IPs.length);
                    // console.log(responses.length);
                    return (IPs.length == responses.length);
                })
                .done(function (result) {
                    // do stuff
                    callback(null, 'ok');
                });
        }
    }
};