/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */

module.exports.controller = (req, res) => {
    res.render('controller', { req, res, active: 'controller', title: '' });
};

module.exports.loginPage = (req, res) => {
    res.render('login/login', { req, res, active: 'login', title: 'Login' });
};

/*
module.exports.forgetPasswordPage = (req, res) => {
    res.render('login/forgot-password', { req, res });
};

module.exports.registerPage = (req, res) => {
    res.render('login/register', { req, res });
};*/